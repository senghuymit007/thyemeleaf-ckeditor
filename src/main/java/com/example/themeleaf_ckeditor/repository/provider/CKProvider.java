package com.example.themeleaf_ckeditor.repository.provider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CKProvider {
    public String getAdmin(@Param("id") Integer id) {
        id = null;
        Integer finalId = id;
        String sql = new SQL() {{
            SELECT("*");
            FROM("admin");
            if (finalId == null) {
                System.out.println("Id is null");
            } else {
                WHERE("id = 1");
            }
            ORDER_BY("id");
            OFFSET(0);
        }}.toString();
        System.out.println(sql);
        return sql;
    }
}
