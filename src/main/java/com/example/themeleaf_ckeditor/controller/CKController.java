package com.example.themeleaf_ckeditor.controller;

import com.example.themeleaf_ckeditor.model.CkText;
import com.example.themeleaf_ckeditor.service.CKService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/ckeditor")
public class CKController {

    @Autowired
    private CKService ckService;

    @GetMapping("/")
    public String getText(Model model){
        List<CkText> ckTexts = ckService.getAllAdmin();
        System.out.println(ckTexts);
        model.addAttribute("admins",ckTexts);
        return "index";
    }

    @PostMapping("/ck-post")
    public String postText(@ModelAttribute CkText ckText) {
        ckText.setId((int)(Math.random() * 100));
        ckService.insertAdmin(ckText);
        return "redirect:/ckeditor/";
    }

    @GetMapping("/ck-post")
    public String create(Model model) {
        model.addAttribute("admin", new CkText());
        return "create_ckeditor";
    }

}
