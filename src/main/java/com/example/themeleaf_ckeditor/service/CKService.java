package com.example.themeleaf_ckeditor.service;

import com.example.themeleaf_ckeditor.model.CkText;

import java.util.List;

public interface CKService {
    CkText findAdminById(int id);
    List<CkText> getAllAdmin();
    boolean insertAdmin(CkText ckText);
}
